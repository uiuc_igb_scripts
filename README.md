# Don's Cluster Scripts

This repo contains three tools which you might find useful; but
really, you probably just want it for `dqsub`.

## dqsub -- qsub or sbatch generator

Use this script to generate qsub scripts. You can run it like:

`dqsub -q default --mem 40G --ppn 12 make foo`

or

`ls -1 */|dqsub -q budget --mem 5 --ppn 6 --array chdir make`

See its documentation for more details.

## fpkm_from_srr

Quick script to get FPKMs from an SRR. Don't actually use this
anymore.

## run_make

Run make with some wrapper options; mainly useful so you can do

`qsub -v MAKE_OPTS=foo -l ppn:5,mem:40G run_make`

but this is largely superseded by `dqsub`.
